package com.modules;


import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.pages.HomePage;
import com.relevantcodes.extentreports.LogStatus;
import com.utility.Configuration;


public class HomeMethods {
	HomePage homePage =new HomePage(Configuration.driver);


	 
	
	public void loginScreen() 
	{
		Configuration.logger=Configuration.extent.startTest("Login Page");
		Configuration.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		if(homePage.getPageTitle().equalsIgnoreCase("nhance"))
		{
			Assert.assertEquals(homePage.getPageTitle(), "nhance");
			Configuration.logger.log(LogStatus.PASS,"User able to see the "+homePage.getPageTitle()+" as a main title ");

		}
		else
		{
			Configuration.logger.log(LogStatus.FAIL,"User able to see the "+homePage.getPageTitle()+" as a main title ");

		}
		Configuration.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		Configuration.logger.log(LogStatus.INFO,"User entered into Login Page");
		Configuration.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		String title=homePage.getTitle();
		if(title.equalsIgnoreCase("Admin Login"))
		{
			Assert.assertEquals(homePage.getTitle(), "Admin Login");
			Configuration.logger.log(LogStatus.PASS,"User able to see the Admin Login as a title ");

		}
		else
		{
			Configuration.logger.log(LogStatus.FAIL,"User able to see the "+title+" as a title ");

		}
		
		Configuration.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		homePage.userName("ann");
		homePage.password("qwertyu");
		homePage.logIn();
		Configuration.driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		Configuration.logger.log(LogStatus.INFO, "User logged successfully");
		Configuration.extent.flush();

	}
	
	public void homePage()
	{
		Configuration.logger=Configuration.extent.startTest("Home Page");
		Configuration.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		List<WebElement> element=homePage.webElements();
		for(WebElement e :element)
		{
			Configuration.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			String modules=e.findElement(By.xpath(".//a")).getText().trim();
			System.out.println("Module name :"+modules);
			Configuration.logger.log(LogStatus.INFO, "User able to see the "+modules+" module");
			Configuration.driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			if(modules.equalsIgnoreCase("CONTENT"))
			{
				Configuration.driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				e.click();
				Configuration.logger.log(LogStatus.INFO, "User Selelected "+modules+" for the process");
				Configuration.driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				String subModules=e.findElement(By.xpath(".//ul/li/a")).getText().trim();
				Configuration.logger.log(LogStatus.INFO, "User Selelected "+subModules+" for the process");
				Configuration.driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				e.findElement(By.xpath(".//ul/li/a")).click();
				Configuration.driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				Configuration.logger.log(LogStatus.INFO, "User able to see the all "+subModules+" Posts");
				Configuration.driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

			}
		}
		
		Configuration.extent.flush();

	}
	public void createPost()
	{
		Configuration.driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		Configuration.logger=Configuration.extent.startTest("Craete Post");
		Configuration.driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		homePage.createPost();
		Configuration.driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		
		if(homePage.cpTitle().equalsIgnoreCase("Create Post"))
		{
			Configuration.driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			Assert.assertEquals(homePage.cpTitle(), "Create Post");
			Configuration.logger.log(LogStatus.PASS, "User able to see the "+homePage.cpTitle()+" as a Header");
			Configuration.driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		}
		else
		{
			Configuration.logger.log(LogStatus.FAIL,"User able to see the "+homePage.cpTitle()+" as a Header ");
		}
		Configuration.driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		homePage.postName("Test Discount 1");
		Configuration.driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		homePage.storeName();
		Configuration.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		homePage.store_Name();		
		Configuration.logger.log(LogStatus.INFO, "User selected stored name");
		homePage.fileBtn(System.getProperty("user.dir")+"//Images//download.jpg");
		Configuration.logger.log(LogStatus.INFO, "User selected Image");
		Configuration.driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		homePage.fromDate();
		Configuration.driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		homePage.nextBTN();
		Configuration.driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		homePage.selectDate();
		Configuration.driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		homePage.toDate();
		Configuration.driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		homePage.nexttBTN();
		Configuration.driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		homePage.nexttBTN();
		Configuration.driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		homePage.selecttsDate();
		Configuration.logger.log(LogStatus.INFO, "User selected FROM  and TO date");
		Configuration.driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		homePage.location("Hyderabad");
		Configuration.logger.log(LogStatus.INFO, "User entered Hyderbad location");
		Configuration.driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		homePage.is_active();
		Configuration.logger.log(LogStatus.INFO, "User Selected Active status");

		Configuration.driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		homePage.submitForm();
		Configuration.logger.log(LogStatus.INFO, "User Created Post");
		Configuration.extent.flush();

		
	}
	

}
