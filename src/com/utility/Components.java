package com.utility;

import org.openqa.selenium.By;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.LogStatus;

public class Components {
	
    @BeforeSuite
    public void beforeSuite() throws InterruptedException {
    	
    	Configuration.extent = new ExtentReports(System.getProperty("user.dir")+"//Report//Keubik.html", true);
    	Configuration.logger=Configuration.extent.startTest(" Application Loading");
    	System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"//drivers//chromedriver.exe");
		ChromeOptions options = new ChromeOptions(); 
		options.addArguments("disable-infobars");
        Configuration.driver= new ChromeDriver(options);
        Configuration.driver.get("https://staging.nhancego.com");
        Configuration.driver.manage().window().maximize();
        Configuration.logger.log(LogStatus.INFO,"****************Open the URL*******************");
		Thread.sleep(7000);

		Configuration.extent.flush();

    }
 /* @AfterSuite
	public void logout() throws InterruptedException
	{
		Thread.sleep(1500);
		Configuration.driver.close();
		Configuration.logger.log(LogStatus.INFO,"*******************Application is logout*******************");
		
	}

    @AfterSuite
    public void afterSuite() {
    	Configuration.logger.log(LogStatus.INFO,"Application going to close");
        Configuration.driver.quit();
        
        
    }*/
    
   

}
