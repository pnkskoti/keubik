package com.utility;
import org.openqa.selenium.WebDriver;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.utility.Configuration;

public class Configuration {
	
     public static ExtentReports extent;
	 public static ExtentTest logger;
	 public static WebDriver driver;
	
}
