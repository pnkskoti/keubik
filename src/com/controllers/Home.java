package com.controllers;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.modules.HomeMethods;
import com.utility.Configuration;

public class Home {
	HomeMethods homeMethods ;
	
	
	@Test(groups={"Home"})
	@Parameters("Home")
	public void home() throws InterruptedException   
	{
		homeMethods=new HomeMethods();
		homeMethods.loginScreen();
		homeMethods.homePage();
		homeMethods.createPost();
		Configuration.extent.flush();
	}
}
