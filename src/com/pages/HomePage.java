package com.pages;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.utility.Configuration;

public class HomePage {

	
	//Loactors
			//GetText
			@FindBy(xpath="//*[@id='content']/div/div/div/div[2]")
			WebElement title;
			//Username
			@FindBy(id="username")
			WebElement Username;
			//Password
			@FindBy(id="password")
			WebElement password;
			//Login
			@FindBy(xpath="//*[@id='content']/div/div/div/form/div[3]/button")
			WebElement login;
			//Get all modules
			@FindBy(xpath="//*[@id='sidebar']/ul/li")
			List<WebElement> list;
			//add Create post
			@FindBy(xpath="//*[@id='myDiv']/div[1]/a/button")
			WebElement post;
			//Header
			@FindBy(xpath="//*[@id='content']/div[2]/div/div[1]/div/h2")
			WebElement cptitle;
			//Post name
			@FindBy(xpath="/html/body/div[1]/div[2]/div/div/div[2]/div[2]/div/div[2]/div[2]/form/div[1]/div[1]/div/div[1]/div/input")
			WebElement post_name;
			//Store date
			@FindBy(id="store_name")
			WebElement sName;
			//Select Store name
			@FindBy(xpath="/html/body/div[1]/div[2]/div/div/div[2]/div[2]/div/div[2]/div[2]/form/div[1]/div[1]/div/div[2]/div/div/div[2]/div[1]")
			WebElement s_Name;
			@FindBy(xpath="//*[@id='post_picture']")
			WebElement file;
			//From Date
			@FindBy(xpath="//*[@id='from_date']/div/span[1]/div/div[1]/input")
			WebElement fDate;
			//next
			@FindBy(xpath="//*[@id='from_date']/div/span[1]/div/div[2]/header/span[3]")
			WebElement next;
			//select date
			@FindBy(xpath="//*[@id='from_date']/div/span[1]/div/div[2]/div/span[11]")
			WebElement date;
			//To Date
			@FindBy(xpath="//*[@id='to_date']/div/span[1]/div/div[1]/input")
			WebElement tdate;
			//next
			@FindBy(xpath="//*[@id='to_date']/div/span[1]/div/div[2]/header/span[3]")
			WebElement tnext;
			//select date
			@FindBy(xpath="//*[@id='to_date']/div/span[1]/div/div[2]/div/span[13]")
			WebElement ts_date;
			//Location
			@FindBy(xpath="/html/body/div[1]/div[2]/div/div/div[2]/div[2]/div/div[2]/div[2]/form/div[1]/div[1]/div/div[5]/div/input")
			WebElement loc;
			//Active
			@FindBy(xpath="//*[@id='is_active']/div[1]/label")
			WebElement act;
			//Submit
			@FindBy(xpath="//*[@id='content']/div[2]/div/div[2]/div[2]/form/div[2]/div/button")
			WebElement create;
			
			
			public String getPageTitle() {
				String pageTitle = Configuration.driver.getTitle();
				return pageTitle;
				
			}	
		
		 public String getTitle()
			{
			 return title.getText().trim();
			}
		 public void userName(String name)
		 {
			 Username.sendKeys(name);
		 }
		 public void password(String pwd)
		 {
			 password.sendKeys(pwd);
		 }
		 public void logIn()
			{
			 login.click();
			}
		 public List<WebElement> webElements()
		 {
			 return list;
		 }
		
		 public void createPost()
		 {
			 post.click();
		 }
		 public String cpTitle()
		 {
			 return cptitle.getText().trim();
		 }
		 public void postName(String pNmae)
		 {
			 post_name.sendKeys(pNmae);
		 }
		 public void storeName()
		 {
			 sName.click();
		 }
		 public void store_Name() {
			 s_Name.click();
			}  
		 public void fileBtn(String img) {
			 file.sendKeys(img);
			}  
		 public void fromDate()
		 {
			 fDate.click();
		 }
		 
		 
		 public void nextBTN()
		 {
			 next.click();
		 }
		 public void selectDate()
		 {
			 date.click();
		 }
		 public void toDate()
		 {
			 tdate.click();
		 }
		 
		 
		 public void nexttBTN()
		 {
			 tnext.click();
		 }
		 public void selecttsDate()
		 {
			 ts_date.click();
		 }
		 
		 public void location(String location1)
		 {
			  loc.sendKeys(location1);
		 }
		 public void is_active()
		 {
			 act.click();
		 }
		 public void submitForm()
		 {
			 create.click();
			 
		 }
		 
		 
		//*[@id="is_active__BV_option_0_"]
		 
		 //Following the page object model
		 public HomePage(WebDriver driver){

		        Configuration.driver= driver;

		        //This initElements method will create all WebElements

		        PageFactory.initElements(driver, this);

		    }

		 

	
	}
